## Add a config.ini file:

```
[CREDENTIALS]
username=<Camera username>
password=<Camera password>

[CAMERA_1]
ip=<Camera 1 IP>
port=<Camera 1 port>
```