import socket
import re
import os
import configparser


config = configparser.ConfigParser()
config.read(os.path.join(os.path.dirname(__file__), 'config.ini'))
username = config['CREDENTIALS']['username']
password = config['CREDENTIALS']['password']
camera_ip = config['CAMERA_1']['ip']
camera_port = int(config['CAMERA_1']['port'])

rtsp_url_1 = "rtsp://{}:{}@{}:{}/0/profile1/media.smp".format(username, password, camera_ip, camera_port)
client_ports = [60784, 60785]  # the client ports we are going to use for receiving video
file_name = "stream.xml"
repeat_count = 50  # receive this many packets

describe_command = "DESCRIBE {} RTSP/1.0\r\n".format(rtsp_url_1) \
    + "CSeq: 2\r\n" \
    + "User-Agent: python\r\n" \
    + "Accept: application/sdp\r\n\r\n"

setup_command = "SETUP {}/trackID=m RTSP/1.0\r\n".format(rtsp_url_1) \
    + "CSeq: 3\r\n" \
    + "User-Agent: python\r\n" \
    + "Transport: RTP/AVP;unicast;client_port={}-{}\r\n\r\n".format(client_ports[0], client_ports[1])

play_command = "PLAY {} RTSP/1.0\r\n".format(rtsp_url_1) \
    + "CSeq: 4\r\n" \
    + "User-Agent: python\r\n" \
    + "Session: {}\r\n" \
    + "Range: npt=0.000-\r\n\r\n"

teardown_command = "TEARDOWN {} RTSP/1.0\r\n".format(rtsp_url_1) \
    + "Cseq: 5\r\n" \
    + "Session: {}\r\n\r\n"


def get_ports(keyword, received_string):
    pattern = re.compile(keyword+"=\d*-\d*")
    matches = pattern.findall(received_string)[0]  # matched string .. "client_port=1000-1001"
    pattern = re.compile('\d+')
    ports = pattern.findall(matches)
    return [int(port) for port in ports]


def print_received(received_string):
    received_strings = received_string.split('\r\n')
    for item in received_strings:
        print(item)


def get_session_id(received_string):
    pattern = re.compile("Session: \d*")
    matches = pattern.findall(received_string)[0]  # matched string .. "client_port=1000-1001"
    pattern = re.compile('\d+')
    session_id = pattern.findall(matches)[0]
    return session_id


def send_command(socket, command):
    print("==================================== SENDING COMMAND ====================================")
    print(command)
    socket.send(command.encode())
    received_string = bytes.decode(socket.recv(4096))
    print("======================================== RECEIVED =======================================")
    print_received(received_string)
    return received_string


sending_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
sending_socket.connect((camera_ip, camera_port))

send_command(sending_socket, describe_command)  # DESCRIBE

received_string = send_command(sending_socket, setup_command)  # SETUP
session_id = get_session_id(received_string)
client_ports = get_ports("client_port", received_string)


listening_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
listening_socket.bind(("", client_ports[0]))
listening_socket.settimeout(5)


play_command = play_command.format(session_id)
send_command(sending_socket, play_command)

file = open(file_name, 'wb')
try:
    for i in range(repeat_count):
        recst = listening_socket.recv(4096)
        print("read", len(recst), "bytes")
        file.write(recst)
        file.flush()
except Exception as e:
    print(e)
file.close()
listening_socket.close()

teardown_command = teardown_command.format(session_id)
send_command(sending_socket, teardown_command)
sending_socket.close()
